<?php

namespace App\Form;

use App\Entity\Article;


use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class, array(
                'label' => 'Titre', 
                'attr' => array('style' => 'width: 100%')
               ))
            ->add('auteur', TextType::class, array(
                'label' => 'Auteur', 
                'attr' => array('style' => 'width: 100%'),
                'required'=> false
               ))
            ->add('date', DateType::class, [
                'widget' => 'single_text',
                'required'=> false,
            ])
            ->add('contenu', TextareaType ::class, array(
                'attr' => array('style' => 'width: 100%'),
               ))
            ->add('imageFile',VichImageType::class,['required'=> false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
