<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
/* Component */
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
/* Repo */
use App\Repository\ArticleRepository;
/* Form */
use App\Form\ArticleType;
/* Entity */
use App\Entity\Article;

class BlogController extends AbstractController
{
	/**
	 * @param ArticleRepository $articleRepository
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function index(ArticleRepository $articleRepository)
    {
		$articles = $articleRepository->findAll();
		return $this->render('list.html.twig', ['articles'=>$articles]);
	}


	/* Lecture seul */
	/**
	 * @param ArticleRepository $articleRepository
	 * @param Article $article
	 */
	public function article(ArticleRepository $articleRepository,Article $article,Request $request){
		
		$blogPost = $articleRepository->findOneById($article);

		if (!$blogPost) {
			$this->addFlash('error', 'Unable to find entry!');
	
			return $this->redirectToRoute('list.blog');
		}
	
		return $this->render('article.html.twig', array(
			'article' => $blogPost
		));
	}

	/* Ajout */
	/**
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function add(Request $request){
		
		$articles = new Article();
		
		$form = $this->createForm(ArticleType::class, $articles);
		//dump( $student);die;
		$form = $form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){

			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($articles);
			$entityManager->flush();
			
			return $this->redirectToRoute('list.blog');
		}
		
		return $this->render('add.html.twig',[
			'form'=>$form->createView()
		]);
	}
	/* Edition */
	/**
	 * @param Article $article
	 */
	public function edit(Article $article,Request $request){
		
		$form = $this->createForm(ArticleType::class, $article);
		
		$form = $form->handleRequest($request);
		
		if($form->isSubmitted() && $form->isValid()){
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->flush();
			
			return $this->redirectToRoute('list.blog');
		}
		
		return $this->render('edit.html.twig',[
			'form'=>$form->createView()
		]);
	}
	/* Suppresion */
	/**
	 * @param Article $article
	 */
	public function delete(Article $article, Request $request){
		$submittedToken = $request->request->get('_token');
		if ($this->isCsrfTokenValid('delete-item', $submittedToken)) {
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->remove($article);
			$entityManager->flush();
			return $this->redirectToRoute('list.blog');
		}else{
			
			dump($student);die;
			return $this->redirectToRoute('list.blog');

		}
	}

}
